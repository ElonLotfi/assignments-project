# Assignments <img src="https://media.giphy.com/media/Wsju5zAb5kcOfxJV9i/giphy.gif" width="170x"> <br /> 

<img src="http://www.ituniversity-mg.com/images/logo_mbds.jpg" width="400px" align="right">


<br />

## Participant :
| *Nom et Prenom* | *Pseudo* |
| ------ | ------ |
| M'hamed Lotfi | ElonLotfi |
| Saidan Rachid | Rachid |


---


Bonjour, <br /> 

Collaborateur du projet :<br>

🙍🏽‍♂️ [@Lotfi](https://github.com/ElonLotfi) <br>
🙍🏽‍♂️ [@Rachid](https://github.com/Rachid?) <br>

encadrant 👨🏽‍💼[@M.Buffa] <br /> 

<br /> 
<br /> 

## Objectif de l'application :
Créer une plateforme pour gérer les assignements des etudiants <br>

## Fonctionnalités implémentées  <img src="https://media.giphy.com/media/26vwfMVM6nlEkwftUj/giphy.gif" width="40px"> <br /> 
✅ Améliorer l'affichage des Assignments(pagination,drag-drop) <br>
✅ Afficher les détails d'un assignements<br>
✅ Afficher les Assignments sous forme de liste de material cards dans deux onglets séparés avec drag-drop<br>
✅ Utiliser un Formulaire de type Stepper (formulaire en plusieurs étapes) pour l'ajout d'Assignments <br>
✅ Ajouter de nouvelles propriétés au modèle des Assignments (Auteur,Matière,Note,Remarques) <br>
✅ Ajouter et editer un assignement <br>
✅ Gérer la sécurité et authentification grâce à JWT <br>
✅ Ajouter une page de login et une page d'inscriprtion <br>
✅ Utilisation de Route Guards <br>
✅ Utilisation de Faker pour ajouter de faux utilisateurs dans ma base de données <br>
✅ Réaliser un script nodeJS qui génère 1000 assignements avec des données aléatoires <br>
✅ Hébergement sur Heroku.com <br>


## Video <br />

[![Watch the video](https://gitlab.com/ElonLotfi/assignments-project/-/raw/main/assignement.png)](https://youtu.be/u39oaGjE1Gs)



## Lien de l'application sur herokus <br />

[https://jughead-apps.herokuapp.com/](https://jughead-apps.herokuapp.com/) <br>

## Une application Web develppé à l'aide de:<br>
 `Angular & NodJS`<br>







