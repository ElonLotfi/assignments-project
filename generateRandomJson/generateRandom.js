var faker = require('faker');
//Permet de faire l'import
// mongoimport --uri mongodb+srv://user:user@premium.hsosy.mongodb.net/premium --collection assignments --file data.json --jsonArray


var imageProf = ["https://i1.rgstatic.net/ii/profile.image/712495153029121-1546883490651_Q512/Michel-Buffa.jpg",
"https://avatars.githubusercontent.com/u/6582651?v=4",
"https://media-exp1.licdn.com/dms/image/C5603AQHg13iSRaqznA/profile-displayphoto-shrink_200_200/0/1516485131056?e=1645660800&v=beta&t=soZc1p-xu_Mec6Y9hI4hGJqldfWMhHR5xosz12t1KhM",
"https://static9.depositphotos.com/1070812/1091/i/950/depositphotos_10916856-stock-photo-teacher-on-background-of-blackboard.jpg",
"https://static8.depositphotos.com/1258191/903/i/950/depositphotos_9033860-stock-photo-portrait-of-a-young-woman.jpg",
"https://st.depositphotos.com/1000393/2567/i/950/depositphotos_25677935-stock-photo-teacher.jpg",
"https://image.shutterstock.com/image-photo/asian-senior-male-calculus-professor-260nw-1891943233.jpg"
]

var nomDevoirs = ["Travaux pratiques", "Examen","Oral","Soutenance finale","Partiel"]




function generateRandomData(numberMax) {

    var data = []
    for (let i = 0; i < numberMax; i++) {

        var randomData = {}
        var randomNameStudent = faker.name.findName();
        var randomDate = faker.date.between("01-01-2021", "12-31-2022").toISOString().split('T')[0]
        randomData["id"] = i + 1
        randomData["nom"] = randomNameStudent
        randomData["dateDeRendu"] = randomDate
        randomData["matiere"]=[]
        let rendu = faker.datatype.boolean()
        randomData["rendu"] = rendu
        if (rendu === true) {
            let note = getRandomInt(20)
            randomData["note"] = note
            if(note < 10) {
                randomData["remarque"] = "Insuffisant, il faut être plus assidu feignasse."
            }
            if(note>10 && note <15){
                randomData["remarque"] = "Vous êtes sur les bons rails, vous pouvez faire mieux dans le prochain examen"
            }
            if(note>15){
                randomData["remarque"] = "résultat honorable, bravo "
            }
        }

        const randomImageProf = Math.floor(Math.random() * imageProf.length);
        var java = {}
        java["nomMatiere"] = "Java"
        java["imageProf"] = imageProf[randomImageProf]
        java["imageMatiere"] = "https://wallpapercave.com/wp/wp7472020.jpg"

        var database = {}
        database["nomMatiere"] = "Database"
        database["imageProf"] = imageProf[randomImageProf]
        database["imageMatiere"] = "https://wallpapercave.com/wp/wp2347580.jpg"

        var javaScript = {}
        javaScript["nomMatiere"] = "JavaScript"
        javaScript["imageProf"] = imageProf[randomImageProf]
        javaScript["imageMatiere"] = "https://wallpapercave.com/wp/wp2465927.jpg"

        var iot = {}
        iot["nomMatiere"] = "Iot"
        iot["imageProf"] = imageProf[randomImageProf]
        iot["imageMatiere"] = "https://wallpapercave.com/wp/wp4902400.jpg"
        var matiere = [java, database, javaScript, iot]

        const random = Math.floor(Math.random() * matiere.length);
        randomData["matiere"].push(matiere[random])
        const randomDevoirs = Math.floor(Math.random() * nomDevoirs.length);
        randomData["nomDevoir"] = nomDevoirs[randomDevoirs]
        data.push(randomData)
    }
    let dataJson = JSON.stringify(data, null, 2)
    exportData(dataJson)

    return data
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function exportData(data) {
    const fs = require('fs')

    const content = data

    fs.writeFile('/home/mbds/Desktop/noel/json/data.json', content, err => {
        if (err) {
            console.error(err)
            return
        }
        //file written successfully
    })
}

console.log(generateRandomData(1000))

